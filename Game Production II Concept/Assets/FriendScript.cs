﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendScript : MonoBehaviour {

	private Transform target;

	public float speed;
	public BoxCollider2D bugHitbox;
	public PlayerScript playerCharacter;
	public BoxCollider2D netHitbox;


	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, target.position, step);
	}
	void OnTriggerEnter2D (Collider2D other){
		Debug.Log ("I have been entered.");
		if (other == netHitbox) {
			gameObject.SetActive (false);
			playerCharacter.bugCounter++;
		}
	}
}

