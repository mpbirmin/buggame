﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

	public BoxCollider2D bugHitbox;
	public PlayerScript playerCharacter;
	public BoxCollider2D netHitbox;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter2D (Collider2D other){
		Debug.Log ("I have been entered.");
		if (other == netHitbox) {
			Destroy (gameObject);
			playerCharacter.bugCounter++;
		}
	}
}
