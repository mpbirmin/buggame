﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	public int bugCounter;
	public bool isNetOut;
	public GameObject net;
	public GameObject friendlyBug;
	public float speed = 5f;

	private Rigidbody2D rb;


	// Use this for initialization
	void Start () {
		bugCounter = 0;
		net.SetActive (false);
		rb = gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxis ("Horizontal");
		float y = Input.GetAxis ("Vertical");
		x = x * Time.deltaTime * speed;
		y = y * Time.deltaTime * speed;

		rb.transform.Translate (x, y, 0f);

		if (Input.GetKeyDown(KeyCode.Space) && !isNetOut) {
			Debug.Log ("The net has been unleashed.");
			StartCoroutine (PullOutNet (net, 0.5f));
			isNetOut = true;

		}
		if (Input.GetKey (KeyCode.Return) && bugCounter > 0 && !isNetOut) {
			friendlyBug.SetActive (true);
			bugCounter--;
		}
	}
	IEnumerator PullOutNet (GameObject bugNet, float delay){
		Debug.Log ("The net's coming, guys!");
		bugNet.SetActive (true);
		yield return new WaitForSeconds (delay);
		bugNet.SetActive (false);
		isNetOut = false;
	}
}
